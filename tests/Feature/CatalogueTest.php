<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Catalogue;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CatalogueTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        Event::fake();

        $response = $this->post('/admin/catalogue/register_catalogue',[
            'name' => 'TestUnitCatalogue',
            'description' => 'Esta es una prueba de Test',
            'state' => '0',
        ]);

        $this->assertCount(1, Catalogue::all());
    }

}
