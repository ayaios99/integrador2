<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->withoutExceptionHandling();
        
        Event::fake();

        $response = $this->post('/admin/user/register_user',[
            'name' => 'TestUnitUser',
            'email' => 'test_unit@test.com',
            'telefono' => '987654321',
            'document' => '64736412',
        ]);

        $this->assertCount(1, User::all());
    }
}
