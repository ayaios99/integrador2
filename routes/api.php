<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/user_register','Api\AuthController@register');

Route::post('/login','Api\AuthController@login');

Route::get('/categories','Api\ArticleController@getAllCategories');

Route::get('/get-articles','Api\ArticleController@get')->middleware('auth:api');

Route::post('/filter_articles','Api\ArticleController@getArticlesfilters')->middleware('auth:api');



Route::get('/get-article-detail/{id}','Api\ArticleController@detail_article')->middleware('auth:api');

Route::get('/myself','Api\AuthController@myself')->middleware('auth:api');

Route::post('/favorites','Api\AuthController@register_favorites')->middleware('auth:api');

Route::get('/get-favorites','Api\AuthController@getFavorites')->middleware('auth:api');

Route::post('/reserve','Api\AuthController@register_reserve')->middleware('auth:api');

Route::get('/get-reserves','Api\AuthController@getReserves')->middleware('auth:api');


Route::post('/edit_myself','Api\AuthController@edit_myself')->middleware('auth:api');