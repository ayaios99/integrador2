<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index');

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');


// ADMIN PAGES //


Route::group(['middleware' => ['auth', 'role:admin']], function(){


Route::get('/admin/welcome','GeneralController@viewAdminWelcome')->name('admin_viewWelcome');

//ROLES//
Route::get('/admin/role/list_role','Admin\RoleController@viewRoles')->name('admin_viewRoles');
Route::get('/admin/role/register_role','Admin\RoleController@viewRegisterRoles')->name('admin_viewRegisterRoles');
Route::post('/admin/role/register_role','Admin\RoleController@registerRoles')->name('admin_RegisterRoles');
Route::get('/admin/role/edit_role/{id}','Admin\RoleController@viewEditRoles')->name('admin_viewEditRoles');
Route::post('/admin/role/edit_role/{id}','Admin\RoleController@editRoles')->name('admin_EditRoles');
Route::get('/admin/role/delete_role/{id}','Admin\RoleController@deleteRoles')->name('admin_DeleteRoles');


//USERS//
Route::get('/admin/user/list_user','Admin\UserController@viewUser')->name('admin_viewUser');
Route::get('/admin/user/register_user','Admin\UserController@viewRegisterUser')->name('admin_viewRegisterUser');
Route::post('/admin/user/register_user','Admin\UserController@registerUser')->name('admin_RegisterUser');
Route::get('/admin/user/edit_user/{id}','Admin\UserController@viewEditUser')->name('admin_viewEditUser');
Route::post('/admin/user/edit_user/{id}','Admin\UserController@editUser')->name('admin_EditUser');
Route::get('/admin/user/delete_user/{id}','Admin\UserController@deleteUser')->name('admin_DeleteUser');

//PROVIDER//
Route::get('/admin/provider/list_provider','Admin\UserController@viewProvider')->name('admin_viewProvider');
Route::get('/admin/provider/register_provider','Admin\UserController@viewRegisterProvider')->name('admin_viewRegisterProvider');
Route::get('/admin/provider/edit_provider/{id}','Admin\UserController@viewEditProvider')->name('admin_viewEditProvider');
Route::post('/admin/provider/register_provider','Admin\UserController@registerUser')->name('admin_RegisterProvider');
Route::post('/admin/provider/edit_provider/{id}','Admin\UserController@editUser')->name('admin_EditProvider');
Route::get('/admin/provider/delete_provider/{id}','Admin\UserController@deleteUser')->name('admin_DeleteProvider');


// BRAND //
Route::get('/admin/brand/list_brand','Admin\BrandController@viewBrand')->name('admin_viewBrand');
Route::get('/admin/brand/register_brand','Admin\BrandController@viewRegisterBrand')->name('admin_viewRegisterBrand');
Route::post('/admin/brand/register_brand','Admin\BrandController@registerBrand')->name('admin_RegisterBrand');
Route::get('/admin/brand/edit_brand/{id}','Admin\BrandController@viewEditBrand')->name('admin_viewEditBrand');
Route::post('/admin/brand/edit_brand/{id}','Admin\BrandController@editBrand')->name('admin_EditBrand');
Route::get('/admin/brand/delete_brand/{id}','Admin\BrandController@deleteBrand')->name('admin_DeleteBrand');


//CATEGORY//
Route::get('/admin/category/list_category','Admin\CategoryController@viewCategory')->name('admin_viewCategory');
Route::get('/admin/category/register_category','Admin\CategoryController@viewRegisterCategory')->name('admin_viewRegisterCategory');
Route::post('/admin/category/register_category','Admin\CategoryController@registerCategory')->name('admin_RegisterCategory');
Route::get('/admin/category/edit_category/{id}','Admin\CategoryController@viewEditCategory')->name('admin_viewEditCategory');
Route::post('/admin/category/edit_category/{id}','Admin\CategoryController@editCategory')->name('admin_EditCategory');
Route::get('/admin/category/delete_category/{id}','Admin\CategoryController@deleteCategory')->name('admin_DeleteCategory');



//CATALOGUE//
Route::get('/admin/catalogue/list_catalogue','Admin\CatalogueController@viewCatalogue')->name('admin_viewCatalogue');
Route::get('/admin/catalogue/register_catalogue','Admin\CatalogueController@viewRegisterCatalogue')->name('admin_viewRegisterCatalogue');
Route::post('/admin/catalogue/register_catalogue','Admin\CatalogueController@registerCatalogue')->name('admin_RegisterCatalogue');
Route::get('/admin/catalogue/edit_catalogue/{id}','Admin\CatalogueController@viewEditCatalogue')->name('admin_viewEditCatalogue');
Route::post('/admin/catalogue/edit_catalogue/{id}','Admin\CatalogueController@editCatalogue')->name('admin_EditCatalogue');
Route::get('/admin/catalogue/delete_catalogue/{id}','Admin\CatalogueController@deleteCatalogue')->name('admin_DeleteCatalogue');


//ARTICLE//
Route::get('/admin/article/list_article','Admin\ArticleController@viewArticle')->name('admin_viewArticle');
Route::get('/admin/article/register_article','Admin\ArticleController@viewRegisterArticle')->name('admin_viewRegisterArticle');

Route::post('/admin/article/register_article','Admin\ArticleController@registerArticle')->name('admin_RegisterArticle');

Route::get('/admin/article/edit_article/{id}','Admin\ArticleController@viewEditArticle')->name('admin_viewEditArticle');

Route::post('/admin/article/edit_article/{id}','Admin\ArticleController@editArticle')->name('admin_EditArticle');

Route::get('/admin/article/delete_article/{id}','Admin\ArticleController@deleteArticle')->name('admin_DeleteArticle');

});

// END ADMIN PAGES //




// PROVEEDOR PAGES //
Route::group(['middleware' => ['auth', 'role:proveedor']], function(){

Route::get('/provider/welcome','GeneralController@viewProviderWelcome')->name('provider_viewWelcome');

//ARTICLE//
Route::get('/provider/article/list_article','Provider\ArticleController@viewArticle')->name('provider_viewArticle');
Route::get('/provider/article/register_article','Provider\ArticleController@viewRegisterArticle')->name('provider_viewRegisterArticle');

Route::post('/provider/article/register_article','Provider\ArticleController@registerArticle')->name('provider_RegisterArticle');

Route::get('/provider/article/edit_article/{id}','Provider\ArticleController@viewEditArticle')->name('provider_viewEditArticle');

Route::post('/provider/article/edit_article/{id}','Provider\ArticleController@editArticle')->name('provider_EditArticle');

Route::get('/provider/article/delete_article/{id}','Provider\ArticleController@deleteArticle')->name('provider_DeleteArticle');

});
// END PROVEEDOR PAGES //




// FUNCTIONS ROUTES //

// END FUNCTIONS PAGES //