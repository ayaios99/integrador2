<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReserveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reserve', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reason')->nullable();
            $table->dateTime('reserve_max_date');
            $table->boolean('state');

            $table->integer('id_article')->unsigned()->index()->nullable();
            $table->foreign('id_article')->references('id')->on('article');

            $table->integer('id_user')->unsigned()->index()->nullable();
            $table->foreign('id_user')->references('id')->on('users');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reserve');
    }
}
