<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('address')->nullable();
            $table->string('telefono')->nullable();
            $table->string('document');

            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('password_mobile');
            $table->rememberToken();

            $table->integer('id_rol')->unsigned()->index()->nullable();
            $table->foreign('id_rol')->references('id')->on('role');

            
            $table->integer('id_store')->unsigned()->index()->nullable();
            $table->foreign('id_store')->references('id')->on('store');

            $table->timestamps();
            $table->softDeletes();
           

            
             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
