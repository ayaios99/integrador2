<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likes', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active');

            $table->integer('id_user')->unsigned()->index()->nullable();
            $table->foreign('id_user')->references('id')->on('users');

            $table->integer('id_article')->unsigned()->index()->nullable();
            $table->foreign('id_article')->references('id')->on('article');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likes');
    }
}
