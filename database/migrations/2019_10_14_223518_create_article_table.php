<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->integer('price');
            $table->boolean('state');
            $table->integer('stock');
            $table->string('img_url',300)->nullable();

            $table->integer('id_user')->unsigned()->index()->nullable();
            $table->foreign('id_user')->references('id')->on('users');

            $table->integer('id_brand')->unsigned()->index()->nullable();
            $table->foreign('id_brand')->references('id')->on('brand');

            $table->integer('id_category')->unsigned()->index()->nullable();
            $table->foreign('id_category')->references('id')->on('category');

            $table->integer('id_catalogue')->unsigned()->index()->nullable();
            $table->foreign('id_catalogue')->references('id')->on('catalogue');

            $table->timestamps();
            $table->softDeletes();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article');
    }
}
