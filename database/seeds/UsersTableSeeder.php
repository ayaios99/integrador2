<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = array(
            array('name'=>'admin',
                'description'=> 'Este es el campo Admin'
        ),
            array('name'=>'proveedor',
                'description'=> 'Este es el campo Proveedor'
        ),
            array('name'=>'cliente',
                'description'=> 'Este es el campo Usuario'
        ),


        );

         DB::table('role')->insert($roles);

         DB::table('users')->insert(
            array(
                'name' => 'Iosef Ayala',
                'email' => 'admin@informatec.pe',
                'telefono' => '987654321',
                'document' => '74716955',
                'password'=> bcrypt('123456789'),
                'password_mobile'=> '123456789',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                   'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                   'id_rol' => '1',
            )

        );

         DB::table('users')->insert(
            array(
                'name' => 'Freddy Choquecota',
                'address'=> 'Av. Santa Rosa 529',
                'email' => 'freddyCho@informatec.pe',
                'telefono' => '987654321',
                'document' => '12345678',
                'password'=> bcrypt('123456789'),
                'password_mobile'=> '123456789',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                   'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                   'id_rol' => '2',
            )
        );

         DB::table('users')->insert(
            array(
                'name' => 'Eduardo Huaman',
                'email' => 'eduardo@informatec.pe',
                'telefono' => '987654321',
                'document' => '87654321',
                'password'=> bcrypt('123456789'),
                'password_mobile'=> '123456789',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                   'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                   'id_rol' => '3',
            )
        );


         DB::table('brand')->insert(
            array(
                'name' => 'Informatec',
                'description' => 'Esta es la marca por defecto de la mismo negocio',
            )
        );

        $category = array(
            array('name'=>'CAMARAS WEB & VIGILANCIA',
                'description'=> 'Campo de categoría Camaras'
        ),
            array('name'=>'CASES & ACCESORIOS',
                'description'=> 'Este es el campo Cases'
        ),
            array('name'=>'CELULARES / TABLET & ACCESORIOS',
                'description'=> 'Este es el campo Celulares y Accesosrios'
        ),

            array('name'=>'CONECTIVIDAD / REDES',
                'description'=> 'Este es el campo Conectividad y Redes'
        ),

            array('name'=>'CONSOLAS / MANDOS / JUEGOS',
                'description'=> 'Este es el campo CONSOLAS / MANDOS / JUEGOS'
        ),


            array('name'=>'DISCOS DUROS & ACCESORIOS',
                'description'=> 'Este es el campo DISCOS DUROS & ACCESORIOS'
        ),

            array('name'=>'FUENTES DE PODER',
                'description'=> 'Este es el campo FUENTES DE PODER'
        ),


            array('name'=>'HOGAR',
                'description'=> 'Este es el campo HOGAR'
        ),

            array('name'=>'IMPRESORAS & ACCESORIOS',
                'description'=> 'Este es el campo IMPRESORAS & ACCESORIOS'
        ),

            array('name'=>'LAPTOP & ACCESORIOS',
                'description'=> 'Este es el campo LAPTOP & ACCESORIOS'
        ),

            array('name'=>'LECTORES & DISPOSITIVOS',
                'description'=> 'Este es el campo LECTORES & DISPOSITIVOS'
        ),

            array('name'=>'LICENCIAS & SOFTWARE',
                'description'=> 'Este es el campo LICENCIAS & SOFTWARE'
        ),

            array('name'=>'MEMORIAS / RAM / USB / SD',
                'description'=> 'Este es el campo MEMORIAS / RAM / USB / SD'
        ),

            array('name'=>'MINI PCS / ALL IN ONE',
                'description'=> 'Este es el campo MINI PCS / ALL IN ONE'
        ),

            array('name'=>'MONITOR / TV & ACCESORIOS',
                'description'=> 'Este es el campo MONITOR / TV & ACCESORIOS'
        ),

            array('name'=>'PLACA MADRE ( MOTHERBOARD )',
                'description'=> 'Este es el campo PLACA MADRE ( MOTHERBOARD )'
        ),

            array('name'=>'PROCESADORES & COMPLEMENTOS',
                'description'=> 'Este es el campo PROCESADORES & COMPLEMENTOS'
        ),

            array('name'=>'PROYECTORES & ACCESORIOS',
                'description'=> 'Este es el campo PROYECTORES & ACCESORIOS'
        ),

            array('name'=>'SUMINISTRO / TINTAS / CARTUCHOS',
                'description'=> 'Este es el campo SUMINISTRO / TINTAS / CARTUCHOS'
        ),

            array('name'=>'TABLETA DIGITALIZADORA',
                'description'=> 'Este es el campo TABLETA DIGITALIZADORA'
        ),

            array('name'=>'TARJETAS DE VIDEO',
                'description'=> 'Este es el campo TARJETAS DE VIDEO'
        ),

            array('name'=>'TECLADOS / MOUSE / PAD MOUSE / KIT',
                'description'=> 'Este es el campo TECLADOS / MOUSE / PAD MOUSE / KIT'
        ),

            array('name'=>'UPS ESTABILIZADOR & SUPRESOR',
                'description'=> 'Este es el campo UPS ESTABILIZADOR & SUPRESOR'
        ),

        );

         DB::table('category')->insert($category);

        DB::table('catalogue')->insert(
            array(
                'name' => 'Informatec Catalogue',
                'state' => '1',
                'description' => 'Esta es el catálogo por defecto de la mismo negocio',
            )
        );

        $article = factory(App\Models\Article::class, 100)->create();
    }
}
