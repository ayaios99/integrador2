<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Article;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => 'Este es un producto de test',
        'price' => $faker->numberBetween($min = 1, $max = 5000),
        'state' => $faker->numberBetween($min = 0, $max = 1),
        'stock'=> $faker->numberBetween($min = 1, $max = 100),
        'img_url'=> 'https://image.shutterstock.com/image-photo/ufa-russia-october-20-2017-260nw-744404161.jpg',
        'id_user'=> 2,
        'id_brand'=> 1,
        'id_category'=> $faker->numberBetween($min = 1, $max = 20),
        'id_catalogue'=> 1,
    ];
});
