@extends('admin.layouts.master')

@section('css')
<link href="{{URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('assets/libs/dropify/dropify.min.css')}}" rel="stylesheet" type="text/css" />

        <link href="{{URL::asset('assets/libs/switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/summernote/summernote-bs4.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Admin</a></li>
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Producto</a></li>
                                    <li class="breadcrumb-item active">Editar</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Editar de Producto</h4>
                        </div>
                    </div>
</div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-body">
                            	@if(session()->has('msj'))

                                                 <div class="alert alert-info alert-dismissable fade show alert-bordered">
                                                 <button class="close" data-dismiss="alert" aria-label="Close"></button><strong>¡Enhorabuena!</strong><br>{{ session('msj')  }}
                                                </div>

                                                @else
                                                @if ($errors->any())
                                                 <div id="error" class="alert alert-danger" role="alert" style="width: 100%;">
                                                @foreach ($errors->all() as $error)
                                                 <span>{{ $error }}</span></br>
                                                @endforeach
                                                </div>
                                                @endif
                                            @endif

                                <h4 class="header-title">Detalle de Category</h4>
                                <p class="sub-header">
                                   Puedes editar las marcas que pueden existir.
                                </p>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <form method="post" action="{{route('admin_EditArticle',$article->id)}}" enctype="multipart/form-data">
                                        	@csrf
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Nombre</label>
                                                <input type="text" id="simpleinput" name="name" value="{{$article->name}}" class="form-control">
                                                <span class="alert {{ $errors->first('name','alert-danger') }}">{{ $errors->first('name',':messages') }}
                                                </span>
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="example-textarea">Descripción</label>
                                                <textarea class="form-control" id="example-textarea" rows="5" name="description">{{$article->description}}</textarea>
                                                <span class="alert {{ $errors->first('description','alert-danger') }}">{{ $errors->first('description',':messages') }}
                                                </span>
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="price">Precio</label>
                                                <input type="text" id="price" name="price" value="{{$article->price}}" class="form-control">
                                                <span class="alert {{ $errors->first('price','alert-danger') }}">{{ $errors->first('price',':messages') }}
                                                </span>
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="stock">Stock</label>
                                                <input type="number" id="stock" name="stock" value="{{$article->stock}}" class="form-control">

                                                <span class="alert {{ $errors->first('stock','alert-danger') }}">{{ $errors->first('stock',':messages') }}
                                                </span>
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="img_url">Ingrese Imagen</label>
                                                <input type="file" class="dropify form-control" data-max-file-size="1M" name="img_url" id="img_url" accept="image/png, .jpeg, .jpg, image/gif"/>

                                                <span class="alert {{ $errors->first('img_url','alert-danger') }}">{{ $errors->first('img_url',':messages') }}
                                                </span>
                                            </div>




                                            <div class="form-group mb-3">
                                                <label for="id_user">Proveedor</label>

                                                <select class="form-control" id="id_user" name="id_user" data-toggle="select2">
                                                    @foreach($user as $item)
                                                    <option value="{{$item->id}}" @if($article->id_user == $item->id) selected='selected' @endif >{{$item->name}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="alert {{ $errors->first('id_user','alert-danger') }}">{{ $errors->first('id_user',':messages') }}
                                                </span>

                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="id_brand">Marca</label>

                                                <select class="form-control" id="id_brand" name="id_brand" data-toggle="select2">
                                                    @foreach($brand as $item)
                                                    <option value="{{$item->id}}" @if($article->id_brand == $item->id) selected='selected' @endif >{{$item->name}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="alert {{ $errors->first('id_brand','alert-danger') }}">{{ $errors->first('id_brand',':messages') }}
                                                </span>

                                            </div>


                                            <div class="form-group mb-3">
                                                <label for="id_category">Categoría</label>

                                                <select class="form-control" id="id_category" name="id_category" data-toggle="select2">
                                                    @foreach($category as $item)
                                                    <option value="{{$item->id}}" @if($article->id_category == $item->id) selected='selected' @endif >{{$item->name}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="alert {{ $errors->first('id_category','alert-danger') }}">{{ $errors->first('id_category',':messages') }}
                                                </span>

                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="id_catalogue">Catálogo</label>

                                                <select class="form-control" id="id_catalogue" name="id_catalogue" data-toggle="select2">
                                                    @foreach($catalogue as $item)
                                                    <option value="{{$item->id}}" @if($article->id_catalogue == $item->id) selected='selected' @endif >{{$item->name}}</option>
                                                    @endforeach
                                                </select>

                                                <span class="alert {{ $errors->first('id_catalogue','alert-danger') }}">{{ $errors->first('id_catalogue',':messages') }}
                                                </span>

                                            </div>


                                            <div class="form-group mb-3">
                                                <label for="state">Estado</label>

                                                <select class="form-control" id="state" name="state" data-toggle="select2">
                                                    <option value="1" @if($article->state == 1) selected='selected' @endif>Activo</option>
                                                    <option value="0" @if($article->state == 0) selected='selected' @endif>Inactivo</option>
                                                </select>
                                                <span class="alert {{ $errors->first('state','alert-danger') }}">{{ $errors->first('state',':messages') }}
                                                </span>
                                            </div>


                                            <a href="{{route('admin_viewArticle')}}" class="btn btn-success waves-effect waves-light"><i class="fe-arrow-left"></i> Regresar a Lista</a>


                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Editar</button>
                                        </form>
                                    </div> <!-- end col -->
                                    <div class="col-lg-6 mt-5 pl-4">
                                        <h4 class="header-title">Imagen de Referencia</h4>
                                        <div class="content_img" style="width: 400px;">
                                            <img src="{{$article->img_url}}" width="100%">
                                        </div>
                                    </div>
                                </div>
                                <!-- end row-->

                            </div> <!-- end card-body -->
                        </div> <!-- end card -->
                    </div><!-- end col -->
                </div>
@endsection

@section('script')
<script src="{{URL::asset('assets/libs/dropzone/dropzone.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/dropify/dropify.min.js')}}"></script>
<script src="{{URL::asset('assets/js/pages/form-fileuploads.init.js')}}"></script>

<script src="{{URL::asset('assets/libs/jquery-nice-select/jquery.nice-select.min.js')}}"></script>
        <script src="{{URL::asset('assets/libs/switchery/switchery.min.js')}}"></script>
        <script src="{{URL::asset('assets/libs/jquery-mockjax/jquery.mockjax.min.js')}}"></script>
        <script src="{{URL::asset('assets/libs/autocomplete/jquery.autocomplete.min.js')}}"></script>
        <script src="{{URL::asset('assets/libs/select2/select2.min.js')}}"></script>
        <script src="{{URL::asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
        <!-- Init js-->
        <script src="{{URL::asset('assets/js/pages/form-advanced.init.js')}}"></script>

        <script src="{{ URL::asset('assets/js/pages/form-summernote.init.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/summernote/summernote-bs4.min.js')}}"></script>
@endsection