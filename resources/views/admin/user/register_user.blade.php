@extends('admin.layouts.master')

@section('css')
@endsection

@section('content')
<div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Admin</a></li>
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Usuarios</a></li>
                                    <li class="breadcrumb-item active">Registro</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Registro de Usuarios</h4>
                        </div>
                    </div>
</div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                            	@if(session()->has('msj'))

                                                 <div class="alert alert-info alert-dismissable fade show alert-bordered">
                                                 <button class="close" data-dismiss="alert" aria-label="Close"></button><strong>¡Enhorabuena!</strong><br>{{ session('msj')  }}
                                                </div>

                                                @else
                                                @if ($errors->any())
                                                 <div id="error" class="alert alert-danger" role="alert" style="width: 100%;">
                                                @foreach ($errors->all() as $error)
                                                 <span>{{ $error }}</span></br>
                                                @endforeach
                                                </div>
                                                @endif
                                            @endif

                                <h4 class="header-title">Formulario de Registro</h4>
                                <p class="sub-header">
                                   Puedes registrar los roles que pueden existir.
                                </p>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <form method="post" action="{{route('admin_RegisterUser')}}">
                                        	@csrf
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Nombre</label>
                                                <input type="text" id="simpleinput" name="name" class="form-control">
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="example-textarea">Email</label>
                                                <input type="text" id="simpleinput" name="email" class="form-control">
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="example-textarea">Teléfono</label>
                                                <input type="text" id="simpleinput" name="telefono" class="form-control">
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="example-textarea">Documento de Identidad</label>
                                                <input type="text" id="simpleinput" name="document" class="form-control">
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="id_rol">Rol de Usuario</label>

                                                <select class="form-control" id="example-select" name="id_rol">
                                                    @foreach($role as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <button type="reset" class="btn btn-danger waves-effect waves-light">Cancelar</button>

                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Registrar</button>
                                        </form>
                                    </div> <!-- end col -->
                                </div>
                                <!-- end row-->

                            </div> <!-- end card-body -->
                        </div> <!-- end card -->
                    </div><!-- end col -->
                </div>
@endsection

@section('script')
@endsection