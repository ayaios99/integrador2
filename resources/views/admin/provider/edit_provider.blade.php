@extends('admin.layouts.master')

@section('css')
@endsection

@section('content')
<div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Admin</a></li>
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Proveedor</a></li>
                                    <li class="breadcrumb-item active">Detalle</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Detalle de Proveedor</h4>
                        </div>
                    </div>
</div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-body">
                            	@if(session()->has('msj'))

                                                 <div class="alert alert-info alert-dismissable fade show alert-bordered">
                                                 <button class="close" data-dismiss="alert" aria-label="Close"></button><strong>¡Enhorabuena!</strong><br>{{ session('msj')  }}
                                                </div>

                                                @else
                                                @if ($errors->any())
                                                 <div id="error" class="alert alert-danger" role="alert" style="width: 100%;">
                                                @foreach ($errors->all() as $error)
                                                 <span>{{ $error }}</span></br>
                                                @endforeach
                                                </div>
                                                @endif
                                            @endif

                                <h4 class="header-title">Formulario de Detalle</h4>
                                <p class="sub-header">
                                   Puedes registrar los roles que pueden existir.
                                </p>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <form method="post" action="{{route('admin_EditUser',$provider->id)}}">
                                        	@csrf
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Nombre</label>
                                                <input type="text" id="simpleinput" name="name" value="{{$provider->name}}" class="form-control">
                                                <span class="alert {{ $errors->first('name','alert-danger') }}">{{ $errors->first('name',':messages') }}
                                                </span>
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="example-textarea">Email</label>
                                                <input type="text" id="simpleinput" name="email" class="form-control" value="{{$provider->email}}">
                                                <span class="alert {{ $errors->first('email','alert-danger') }}">{{ $errors->first('email',':messages') }}
                                                </span>
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="example-textarea">Teléfono</label>
                                                <input type="text" id="simpleinput" name="telefono" class="form-control" value="{{$provider->telefono}}">
                                                <span class="alert {{ $errors->first('telefono','alert-danger') }}">{{ $errors->first('telefono',':messages') }}
                                                </span>
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="example-textarea">Dirección Principal</label>
                                                <input type="text" id="address" name="address" class="form-control" value="{{$provider->address}}">
                                                <span class="alert {{ $errors->first('address','alert-danger') }}">{{ $errors->first('address',':messages') }}
                                                </span>
                                            </div>



                                            <a href="{{route('admin_viewProvider')}}" class="btn btn-success waves-effect waves-light"><i class="fe-arrow-left"></i> Regresar a Lista</a>

                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Editar</button>
                                        </form>
                                    </div> <!-- end col -->
                                </div>
                                <!-- end row-->

                            </div> <!-- end card-body -->
                        </div> <!-- end card -->
                    </div><!-- end col -->
                </div>
@endsection

@section('script')
@endsection