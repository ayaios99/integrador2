@extends('admin.layouts.master')

@section('content')
<div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Inicio</a></li>
                                    <li class="breadcrumb-item active">Welcome</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Bienvenido Admin</h4>
                        </div>
                    </div>
                </div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            	<h4 class="header-title">¿Que es Informatec?</h4>
                <p class="text-muted font-13 mb-4">
                	Proyecto de visualización de productos en stock. 
                </p>
            </div>
        </div>
    </div>
</div>
@endsection