@extends('admin.layouts.master')
@section('css')
<link href="{{ URL::asset('assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Admin</a></li>
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Usuarios</a></li>
                                    <li class="breadcrumb-item active">Listado</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Listado de Usuarios</h4>
                        </div>
                    </div>
                </div>   
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title">¿Que son los Roles?</h4>
                <p class="text-muted font-13 mb-4">
                	Son una caracteristica de Jerarquía dentro del sistema en el cual tiene una representación para cada funcionalidad de los usuarios.<br>
                 	Dentro de esta sección del sistema, ustede como usuario "Administrador" podra visualizar todo un listado con los roles que existen actualmente.
                </p>

                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Description</th>
                            <th>Eliminar</th>
                            <th>Editar</th>
                        </tr>
                    </thead>
                                
                                
                    <tbody>
                    	@foreach($brand as $item)
                        <tr>
                            <td>{{$item->name}}</td>
                            <td>{{$item->description}}</td>
                            <td><a data-toggle="modal" data-target="#con-close-modal" href="#" class="eliminar-btn">Eliminar <i class="fe-trash-2"></i><input type="hidden" class="valor_id" value="{{$item->id}}"></a></td>
                            <td><a href="{{route('admin_viewEditBrand',$item->id)}}" class="editar" >Editar<i class="fe-edit-1"></i></a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Confirmación de Borrado</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                        <div class="modal-body p-4">
                                            <div class="row">
                                                <p>¿Seguro que deseas eliminar este Usuario?</p>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                            <a href="" class="btn btn-danger waves-effect waves-light" id="delete_secure">Eliminar</a>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.modal -->
                                
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col -->
</div>
@endsection

@section('script')
<script src="{{ URL::asset('assets/libs/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/buttons.print.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/pdfmake/vfs_fonts.js')}}"></script>

<script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>
<script type="text/javascript">
            $(function(){
                $('.eliminar-btn').click(function(){
                    var id = $(this).find('input.valor_id').val();
                    var url = "/admin/brand/delete_brand/";
                    var dominio = document.location.origin
                    //console.log(); 
                    $('#delete_secure').attr('href',dominio+url+id);

                });
            });
        </script>
@endsection