<div class="topbar-menu">
    <div class="container-fluid">
        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">

                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-map"></i>Estadísticas <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="#">Ir a ver</a>
                        </li>
                    </ul>
                </li>

                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-map"></i>Roles <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="{{route('admin_viewRoles')}}">Lista</a>
                        </li>
                        <li>
                            <a href="{{route('admin_viewRegisterRoles')}}">Registrar Rol</a>
                        </li>
                    </ul>
                </li>        

                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-map"></i>Usuarios <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="{{route('admin_viewUser')}}">Lista</a>
                        </li>
                        <li>
                            <a href="{{route('admin_viewRegisterUser')}}">Registrar Usuario</a>
                        </li>
                    </ul>
                </li>

                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-map"></i>Proveedor <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="{{route('admin_viewProvider')}}">Lista</a>
                        </li>
                        <li>
                            <a href="{{route('admin_viewRegisterProvider')}}">Registrar Proveedor</a>
                        </li>
                    </ul>
                </li>

                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-map"></i>Marca <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="{{route('admin_viewBrand')}}">Lista</a>
                        </li>
                        <li>
                            <a href="{{route('admin_viewRegisterBrand')}}">Registrar Categoría</a>
                        </li>
                    </ul>
                </li>

                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-map"></i>Categoría <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="{{route('admin_viewCategory')}}">Lista</a>
                        </li>
                        <li>
                            <a href="{{route('admin_viewRegisterCategory')}}">Registrar Categoría</a>
                        </li>
                    </ul>
                </li>

                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-map"></i>Catálogo <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="{{route('admin_viewCatalogue')}}">Lista</a>
                        </li>
                        <li>
                            <a href="{{route('admin_viewRegisterCatalogue')}}">Registrar Catálogo</a>
                        </li>
                    </ul>
                </li>

                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-map"></i>Artículo <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="{{route('admin_viewArticle')}}">Lista</a>
                        </li>
                        <li>
                            <a href="{{route('admin_viewRegisterArticle')}}">Registrar Artículo</a>
                        </li>
                    </ul>
                </li>


            </ul>
            <!-- End navigation menu -->

            <div class="clearfix"></div>
        </div>
        <!-- end #navigation -->
    </div>
    <!-- end container -->
</div>
<!-- end navbar-custom -->