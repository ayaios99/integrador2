@extends('admin.layouts.master')

@section('css')
<link href="{{URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('assets/libs/dropify/dropify.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Admin</a></li>
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Catalogue</a></li>
                                    <li class="breadcrumb-item active">Registro</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Registro de Catalogue</h4>
                        </div>
                    </div>
</div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-body">
                            	@if(session()->has('msj'))

                                                 <div class="alert alert-info alert-dismissable fade show alert-bordered">
                                                 <button class="close" data-dismiss="alert" aria-label="Close"></button><strong>¡Enhorabuena!</strong><br>{{ session('msj')  }}
                                                </div>

                                                @else
                                                @if ($errors->any())
                                                 <div id="error" class="alert alert-danger" role="alert" style="width: 100%;">
                                                @foreach ($errors->all() as $error)
                                                 <span>{{ $error }}</span></br>
                                                @endforeach
                                                </div>
                                                @endif
                                            @endif

                                <h4 class="header-title">Detalle de Category</h4>
                                <p class="sub-header">
                                   Puedes editar las marcas que pueden existir.
                                </p>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <form method="post" action="{{route('admin_EditCatalogue',$catalogue->id)}}" enctype="multipart/form-data">
                                        	@csrf
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Nombre</label>
                                                <input type="text" id="simpleinput" name="name" value="{{$catalogue->name}}" class="form-control">
                                                <span class="alert {{ $errors->first('name','alert-danger') }}">{{ $errors->first('name',':messages') }}
                                                </span>
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="example-textarea">Descripción</label>
                                                <textarea class="form-control" id="example-textarea" rows="5" name="description">{{$catalogue->description}}</textarea>
                                                <span class="alert {{ $errors->first('description','alert-danger') }}">{{ $errors->first('description',':messages') }}
                                                </span>
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="img_url">Ingrese Imagen</label>
                                                <input type="file" class="dropify form-control" data-max-file-size="1M" name="img_url" id="img_url" accept="image/png, .jpeg, .jpg, image/gif"/>

                                                <span class="alert {{ $errors->first('img_url','alert-danger') }}">{{ $errors->first('img_url',':messages') }}
                                                </span>
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="state">Estado</label>

                                                <select class="form-control" id="example-select" name="state">
                                                    <option value="1" @if($catalogue->state == 1) selected='selected' @endif>Activo</option>
                                                    <option value="0" @if($catalogue->state == 0) selected='selected' @endif>Inactivo</option>
                                                </select>
                                                <span class="alert {{ $errors->first('state','alert-danger') }}">{{ $errors->first('state',':messages') }}
                                                </span>
                                            </div>


                                            <a href="{{route('admin_viewCatalogue')}}" class="btn btn-success waves-effect waves-light"><i class="fe-arrow-left"></i> Regresar a Lista</a>

                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Editar</button>
                                        </form>
                                    </div> <!-- end col -->
                                </div>
                                <!-- end row-->

                            </div> <!-- end card-body -->
                        </div> <!-- end card -->
                    </div><!-- end col -->
                </div>
@endsection

@section('script')
<script src="{{URL::asset('assets/libs/dropzone/dropzone.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/dropify/dropify.min.js')}}"></script>
<script src="{{URL::asset('assets/js/pages/form-fileuploads.init.js')}}"></script>
<script src="{{URL::asset('assets/js/pages/form-advanced.init.js')}}"></script>
@endsection