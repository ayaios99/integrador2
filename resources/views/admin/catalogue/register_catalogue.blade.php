@extends('admin.layouts.master')

@section('css')
<link href="{{URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::asset('assets/libs/dropify/dropify.min.css')}}" rel="stylesheet" type="text/css" />

        <link href="{{URL::asset('assets/libs/switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/summernote/summernote-bs4.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Admin</a></li>
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Catálogo</a></li>
                                    <li class="breadcrumb-item active">Registro</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Registro de Catálogo</h4>
                        </div>
                    </div>
</div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                            	@if(session()->has('msj'))

                                                 <div class="alert alert-info alert-dismissable fade show alert-bordered">
                                                 <button class="close" data-dismiss="alert" aria-label="Close"></button><strong>¡Enhorabuena!</strong><br>{{ session('msj')  }}
                                                </div>

                                                @else
                                                @if ($errors->any())
                                                 <div id="error" class="alert alert-danger" role="alert" style="width: 100%;">
                                                @foreach ($errors->all() as $error)
                                                 <span>{{ $error }}</span></br>
                                                @endforeach
                                                </div>
                                                @endif
                                            @endif

                                <h4 class="header-title">Formulario de Registro</h4>
                                <p class="sub-header">
                                   Puedes registrar los roles que pueden existir.
                                </p>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <form method="post" action="{{route('admin_RegisterCatalogue')}}" enctype="multipart/form-data">
                                        	@csrf
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Nombre</label>
                                                <input type="text" id="simpleinput" name="name" class="form-control">
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="example-textarea">Descripción</label>
                                                <textarea class="form-control" id="example-textarea" rows="5" name="description"></textarea>
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="img_url">Ingrese Imagen</label>
                                                <input type="file" class="dropify form-control" data-max-file-size="1M" name="img_url" id="img_url" accept="image/png, .jpeg, .jpg, image/gif"/>
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="state">Estado</label>

                                                <select class="form-control" id="example-select" name="state">
                                                    <option value="1">Activo</option>
                                                    <option value="0">Inactivo</option>
                                                </select>
                                            </div>

                                            <button type="reset" class="btn btn-danger waves-effect waves-light">Cancelar</button>

                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Registrar</button>
                                        </form>
                                    </div> <!-- end col -->
                                </div>
                                <!-- end row-->

                            </div> <!-- end card-body -->
                        </div> <!-- end card -->
                    </div><!-- end col -->
                </div>
@endsection

@section('script')

<script src="{{URL::asset('assets/libs/dropzone/dropzone.min.js')}}"></script>
<script src="{{URL::asset('assets/libs/dropify/dropify.min.js')}}"></script>
<script src="{{URL::asset('assets/js/pages/form-fileuploads.init.js')}}"></script>

        <script src="{{URL::asset('assets/js/pages/form-advanced.init.js')}}"></script>

        <script src="{{ URL::asset('assets/js/pages/form-summernote.init.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/summernote/summernote-bs4.min.js')}}"></script>
@endsection