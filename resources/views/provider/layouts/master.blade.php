<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Link Building DataTrust</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        @include('provider.layouts.head')

    </head>

    <body class="boxed-layout">

        <!-- Navigation Bar-->
        <header id="topnav">
      @include('provider.layouts.topbar')
      @include('provider.layouts.nav-horizontal')
            </header>
        <!-- End Navigation Bar-->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->
            <div class="wrapper">
                <div class="container-fluid">
    @yield('content')
    @include('provider.layouts.footer')    
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
                </div> <!-- end wrapper -->
            </div>
            <!-- end wrapper -->
    @include('provider.layouts.footer-script')    
    </body>
</html>