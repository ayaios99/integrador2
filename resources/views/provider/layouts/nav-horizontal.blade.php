<div class="topbar-menu">
    <div class="container-fluid">
        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">

                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-map"></i>Estadísticas <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="#">Ir a ver</a>
                        </li>
                    </ul>
                </li>

                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-map"></i>Productos <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="#">Lista</a>
                        </li>
                        <li>
                            <a href="#">Registrar Producto</a>
                        </li>
                    </ul>
                </li>


            </ul>
            <!-- End navigation menu -->

            <div class="clearfix"></div>
        </div>
        <!-- end #navigation -->
    </div>
    <!-- end container -->
</div>
<!-- end navbar-custom -->