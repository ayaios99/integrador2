<!-- Footer Start -->
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                2019 - {{date('Y')}} &copy; Scotia Agencias by <a href="">Data</a> 
            </div>
            <div class="col-md-6">
                <div class="text-md-right footer-links d-none d-sm-block">
                    
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- end Footer -->