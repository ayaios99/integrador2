<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Catalogue extends Model
{
     use SoftDeletes;
	
    protected $table='catalogue';
    protected $fillable = [
        'name','description', 'state', 'img_url',
    ];

    public function article(){
    	return $this->hasMany('App\Models\Article','id_catalogue');
    }

}
