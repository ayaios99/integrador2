<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    use SoftDeletes;
	
    protected $table='brand';
    protected $fillable = [
        'name','description',
    ];

    public function article(){
    	return $this->hasMany('App\Models\Article','id_brand');
    }
}
