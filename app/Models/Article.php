<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{

	use SoftDeletes;

    protected $table='article';

    protected $fillable = [
        'name','description','price', 'state', 'stock', 'img_url', 'id_user', 'id_brand', 'id_category', 'id_catalogue',
    ];

    public function user(){
        return $this->belongsTo('App\User','id_user');
    }


    public function brand(){
    	return $this->belongsTo('App\Models\Brand','id_brand');
    }

    public function category(){
    	return $this->belongsTo('App\Models\Category','id_category');
    }

    public function catalogue(){
    	return $this->belongsTo('App\Models\Catalogue','id_catalogue');
    }

    public function likes(){
    	return $this->hasMany('App\Models\Likes','id_article');
    }

    public function favorite(){
        return $this->hasMany('App\Models\Favorite','id_article');
    }

    public function reserve(){
        return $this->hasMany('App\Models\Reserve','id_article');
    }

    public function ScopeFilterCategories($query,$categories=null){

                                if($categories){
                                    $query->where('id_category','=',$categories);

                }
            }

    public function ScopeFilterValue($query,$minValue,$maxValue){

                            if($minValue){

                                $query->where('price', '>=', "$minValue");

                                 }

                            if($maxValue){

                                $query->where('price', '<=', "$maxValue");

                                 }

                                 return $query;
                            }
}
