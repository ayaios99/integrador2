<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Store extends Model
{
    use SoftDeletes;
    protected $table='store';

    protected $fillable = [
    	'address', 'long', 'lat', 'reference',
    ];

     public function user(){
    	return $this->hasMany('App\Models\User','id_store');
    }
}
