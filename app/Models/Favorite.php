<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Favorite extends Model
{
    use SoftDeletes;
	
    protected $table='favorite';
    protected $fillable = [
        'id', 'id_user', 'id_article',
    ];

    public function user(){
        return $this->belongsTo('App\Models\User','id_user');
    }

    public function article(){
        return $this->belongsTo('App\Models\Article','id_article');
    }    
}
