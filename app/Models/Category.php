<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
	
    protected $table='category';
    protected $fillable = [
        'name','description','img_url'
    ];

    public function article(){
    	return $this->hasMany('App\Models\Article','id_category');
    }
}
