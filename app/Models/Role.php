<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{

    use SoftDeletes;
    protected $table='role';

    protected $fillable = [
    	'name', 'description'
    ];

     public function user(){
    	return $this->hasMany('App\Models\User','id_rol');
    }
}
