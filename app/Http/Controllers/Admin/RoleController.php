<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Entities\Admin\RolesEntity;
use App\Http\Requests\CreateRole;

use App\Models\Role;

class RoleController extends Controller
{
    public function viewRoles(){
    	$roles = Role::get();
    	return view('admin.roles.list_role',compact('roles'));
    }

    public function viewRegisterRoles(){
    	return view('admin.roles.register_role');
    }

    public function registerRoles(CreateRole $request){
    	$roles = RolesEntity::registerRole($request->all());
    	return back()->with('msj','Se ha creado el Rol');
    }

    public function viewEditRoles($id){
        $roles = RolesEntity::viewDetRole($id);
        return view('admin.roles.edit_role',compact('roles'));
    }

    public function editRoles(CreateRole $request,Role $id){
        $roles = RolesEntity::editRoles($request->all(),$id);
        return back()->with('msj','Se realizo la actualización');
    }

    public function deleteRoles(Role $id){
        $roles = RolesEntity::deleteRoles($id);
        return redirect()->action('Admin\RoleController@viewRoles');
    }

}
