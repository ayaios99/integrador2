<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Entities\Admin\CatalogueEntity;
use App\Http\Controllers\Controller;
use App\Models\Catalogue;

class CatalogueController extends Controller
{
    public function viewCatalogue(){
    	$catalogue = Catalogue::get();
    	return view('admin.catalogue.list_catalogue',compact('catalogue'));
    }

    public function viewRegisterCatalogue(){
    	return view('admin.catalogue.register_Catalogue');
    }

    public function registerCatalogue(Request $request){
        $input = $request->all();
        if ($request->hasFile('img_url')) {
                $file = $request->file('img_url');
                $file_extension = $file->getClientOriginalName();
                $destination_path = public_path() . '/catalogue_img/';
                $filename = $file_extension;
                $request->file('img_url')->move($destination_path, $filename);
                $input['img_url'] = asset('/catalogue_img/'.$filename);
            }

    	$catalogue = CatalogueEntity::registerCatalogue($input);
    	return back()->with('msj','Se ha creado la Marca');
    }

    public function viewEditCatalogue($id){
        $catalogue = CatalogueEntity::viewDetCatalogue($id);
        return view('admin.catalogue.edit_Catalogue',compact('catalogue'));
    }

    public function editCatalogue(Request $request,Catalogue $id){

        $input = $request->all();
        
        if ($request->hasFile('img_url')) {
                $file = $request->file('img_url');
                $file_extension = $file->getClientOriginalName();
                $destination_path = public_path() . '/catalogue_img/';
                $filename = $file_extension;
                $request->file('img_url')->move($destination_path, $filename);
                $input['img_url'] = asset('/catalogue_img/'.$filename);

                //return $input;
        }else{
            $input = $request->except('img_url');
            //return $input;
        }

        $catalogue = CatalogueEntity::editCatalogue($input,$id);
        return back()->with('msj','Se realizo la actualización');
    }

    public function deleteCatalogue(Brand $id){
        $catalogue = CatalogueEntity::deleteCatalogue($id);
        return redirect()->action('Admin\CatalogueController@viewCatalogue');
    }
}
