<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Entities\Admin\BrandEntity;
use App\Http\Controllers\Controller;
use App\Models\Brand;

class BrandController extends Controller
{
    public function viewBrand(){
    	$brand = Brand::get();
    	return view('admin.brand.list_brand',compact('brand'));
    }

    public function viewRegisterBrand(){
    	return view('admin.brand.register_brand');
    }

    public function registerBrand(Request $request){
    	$brand = BrandEntity::registerBrand($request->all());
    	return back()->with('msj','Se ha creado la Marca');
    }

    public function viewEditBrand($id){
        $brand = BrandEntity::viewDetBrand($id);
        return view('admin.brand.edit_brand',compact('brand'));
    }

    public function editBrand(Request $request,Brand $id){
        $brand = BrandEntity::editBrand($request->all(),$id);
        return back()->with('msj','Se realizo la actualización');
    }

    public function deleteBrand(Brand $id){
        $brand = BrandEntity::deleteBrand($id);
        return redirect()->action('Admin\BrandController@viewBrand');
    }
}
