<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Entities\Admin\ArticleEntity;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateArticle;
use App\Models\Article;
use App\User;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Catalogue;


class ArticleController extends Controller
{
    public function viewArticle(){
    	$article = Article::with('user','brand','category','catalogue')->get();
    	return view('admin.article.list_article',compact('article'));
    	//return $article;
    }

    public function viewRegisterArticle(){
    	$user = User::where('id_rol',2)->get();
    	$brand = Brand::get();
    	$category = Category::get();
    	$catalogue = Catalogue::get();
    	//return $catalogue;
    	return view('admin.article.register_article',compact('user','brand','category','catalogue'));
    }

    public function registerArticle(CreateArticle $request){

        $input = $request->all();
        if ($request->hasFile('img_url')) {
                $file = $request->file('img_url');
                $file_extension = $file->getClientOriginalName();
                $destination_path = public_path() . '/productos_img/';
                $filename = $file_extension;
                $request->file('img_url')->move($destination_path, $filename);
                $input['img_url'] = asset('/productos_img/'.$filename);
            }

        $article = ArticleEntity::registerArticle($input);

    	return back()->with('msj','Se ha creado el Producto');
    }

    public function deleteArticle(Article $id){
    	$article = ArticleEntity::deleteArticle($id);
        return redirect()->action('Admin\ArticleController@viewArticle');
    }

    public function viewEditArticle($id){
        $user = User::where('id_rol',2)->get();
        $brand = Brand::get();
        $category = Category::get();
        $catalogue = Catalogue::get();
        $article = ArticleEntity::viewDetArticle($id);
        return view('admin.article.edit_article',compact('article','user','brand','category','catalogue'));
    }

    public function editArticle(Request $request,Article $id){


        $input = $request->all();
        
        if ($request->hasFile('img_url')) {
                $file = $request->file('img_url');
                $file_extension = $file->getClientOriginalName();
                $destination_path = public_path() . '/productos_img/';
                $filename = $file_extension;
                $request->file('img_url')->move($destination_path, $filename);
                $input['img_url'] = asset('/productos_img/'.$filename);

                //return $input;
        }else{
            $input = $request->except('img_url');
            //return $input;
        }

        $article = ArticleEntity::editArticle($input,$id);
        return back()->with('msj','Se ha modificado el Producto');
   
    }
}
