<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Entities\Admin\CategoryEntity;
use App\Http\Controllers\Controller;
use App\Models\Category;

class CategoryController extends Controller
{
    public function viewCategory(){
    	$category = Category::get();
    	return view('admin.category.list_category',compact('category'));
    }

    public function viewRegisterCategory(){
    	return view('admin.category.register_category');
    }

    public function registerCategory(Request $request){

        $input = $request->all();
        if ($request->hasFile('img_url')) {
                $file = $request->file('img_url');
                $file_extension = $file->getClientOriginalName();
                $destination_path = public_path() . '/category_img/';
                $filename = $file_extension;
                $request->file('img_url')->move($destination_path, $filename);
                $input['img_url'] = asset('/category_img/'.$filename);
            }


    	$category = CategoryEntity::registerCategory($input);
    	return back()->with('msj','Se ha creado la Categoría');
    }

    public function viewEditCategory($id){
        $category = CategoryEntity::viewDetCategory($id);
        return view('admin.category.edit_category',compact('category'));
    }

    public function editCategory(Request $request,Category $id){

        $input = $request->all();
        
        if ($request->hasFile('img_url')) {
                $file = $request->file('img_url');
                $file_extension = $file->getClientOriginalName();
                $destination_path = public_path() . '/category_img/';
                $filename = $file_extension;
                $request->file('img_url')->move($destination_path, $filename);
                $input['img_url'] = asset('/category_img/'.$filename);
                //return $input;
        }else{
            $input = $request->except('img_url');
            //return $input;
        }

        $category = CategoryEntity::editCategory($input,$id);
        return back()->with('msj','Se realizo la actualización');
    }

    public function deleteCategory(Category $id){
        $category = CategoryEntity::deleteCategory($id);
        return redirect()->action('Admin\CategoryController@viewCategory');
    }
}
