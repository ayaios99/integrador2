<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
Use Auth;
use App\User;
use App\Models\Role;
use App\Http\Controllers\Controller;
use App\Http\Entities\Admin\UserEntity;
use App\Http\Requests\CreateUser;


class UserController extends Controller
{
    public function viewUser(){
    	$user = User::where('id', '!=', auth()->id())->where('id_rol','!=','2')->get();
    	return view('admin.user.list_user',compact('user'));
    }

    public function viewRegisterUser(){

        $role = Role::where('id','!=','2')->get();
    	return view('admin.user.register_user',compact('role'));
    }

    public function registerUser(CreateUser $request){

        if(!$request->get('id_rol')){
            $request->merge(['id_rol' => '2']);
        }

        $request->merge([
            'password' => bcrypt($request->get('document')),
            'password_mobile' => $request->get('document'),
        ]);

    	$user = UserEntity::registerUser($request->all());
    	return back()->with('msj','Se ha creado el usuario');
    }

    public function viewEditUser($id){
        $role = Role::get();
        $user = UserEntity::viewDetUser($id);
        return view('admin.user.edit_user',compact('user','role'));
    }

    public function editUser(Request $request,User $id){
        $user = UserEntity::editUser($request->all(),$id);
        return back()->with('msj','Se realizo la actualización');
    }

    public function deleteUser(User $id){
        $user = UserEntity::deleteUser($id);
        return redirect()->action('Admin\UserController@viewUser');
    }



    /* PROVIDER */

    public function viewProvider(){
        $provider = User::where('id_rol', '=', '2')->get();
        return view('admin.provider.list_provider',compact('provider'));
    }

    public function viewRegisterProvider(){

        return view('admin.provider.register_provider');
    }

    public function viewEditProvider($id){
        $provider = UserEntity::viewDetProvider($id);
        return view('admin.provider.edit_provider',compact('provider'));
    }

}
