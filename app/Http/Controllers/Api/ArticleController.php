<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;

class ArticleController extends Controller
{

	public function get(){
    $articulos=Article::where('state',1)->get();
    return $articulos;
    }

    public function detail_article($id){
        $last_url = Article::where('id',$id)
                                ->with('user.store')
                                ->first();
        return $last_url;
    }

    public function getArticlesfilters(Request $request){
        $articulos=Article::where('state',1)
                        ->FilterCategories($request->categories)
                        ->FilterValue($request->minValue,$request->maxValue)
                        ->get();

         return response()->json(['articulos' => $articulos]);
    }

    public function getAllCategories(){
        $categories = Category::get();
        return response()->json(['categorias' => $categories]);
    }
    
}
