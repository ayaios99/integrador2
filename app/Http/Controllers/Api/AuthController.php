<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\Article;
use App\Models\Favorite;
use App\Models\Reserve;
use DateTime;
use DateInterval;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\CreateUser;
use App\Http\Entities\Admin\UserEntity;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $loginData = $request->validate([
            'email' => 'email|required',
            'password' =>'required',
        ]);

        if(!auth()->attempt($loginData)){
            return response()->json(['message'=>'Datos Incorrecto',401]);
        }

        $tokenResult = auth()->user()->createToken('Personal Access Token')->accessToken;

        return response()->json([
            'app' => 'Informatec',
            'user'=> auth()->user(),
            'access_token' => $tokenResult,
        ]);
    }

    public function register(CreateUser $request)
    {

        //dd($request);
        $request->merge(['id_rol' =>3]);
        $request->merge([
            'password' => bcrypt($request->get('document')),
            'password_mobile' => $request->get('document'),
        ]);
        $user=UserEntity::createClassicUser($request->all());
        return $user;
        
    }


    public function myself(){
        $user = auth()->user();
        return $user;
    }

    public function register_favorites(Request $request){
        $favorites = UserEntity::createAFavorite($request->all());
        return $favorites;
    }

    public function getFavorites(){
        $favorites = Favorite::where('id_user',auth()->user()->id)->with('article.user')->get();
        return $favorites;
    }

    public function register_reserve(Request $request){
        $now = new DateTime();
        $now->format('Y-m-d H:i:s');
        $now->add(new DateInterval('P0Y0M7DT0H0M0S'));
        $now->format('Y-m-d H:i:s');

        $request->merge(['reserve_max_date' =>$now]);
        $request->merge(['state' =>1]);

        $favorites = UserEntity::createAReserve($request->all());

        return $favorites;
    }

    public function edit_myself(Request $request){
        //$user = UserEntity::editUser($request->all(),auth()->user()->id);
        $user = auth()->user();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->telefono = $request->get('telefono');

        $user->save();

        return $user;
    }

    public function getReserves(){
        $reserve = Reserve::where('id_user',auth()->user()->id)->with('article')->get();
        return $reserve;
    }

}
