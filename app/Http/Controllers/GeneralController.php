<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GeneralController extends Controller
{
    public function viewAdminWelcome(){
    	return view('admin.welcome_admin');
    }

    public function viewProviderWelcome(){
    	return view('provider.welcome_provider');
    }
}
