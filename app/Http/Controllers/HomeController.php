<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->isRole('admin')){
            return redirect('/admin/welcome');
    }else if(Auth::user()->isRole('proveedor')){
            return redirect('/provider/welcome');
    }else{
abort(401, 'Esta acción no está autorizada.');
    }
    }
}
