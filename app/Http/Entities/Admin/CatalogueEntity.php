<?php
namespace App\Http\Entities\Admin;
use App\Models\Catalogue;

class CatalogueEntity{
	static function registerCatalogue($request){
		$catalogue = Catalogue::create($request);
		return $catalogue;
	}

	static function viewDetCatalogue($id){
		$catalogue = Catalogue::where('id',$id)->first();
		return $catalogue;
	}

	static function editCatalogue($request,Catalogue $id){
		$id->update($request);
	}

	static function deleteCatalogue(Catalogue $id){
		$id->delete();
	}
}