<?php
namespace App\Http\Entities\Admin;
use App\Models\Category;

class CategoryEntity{
	static function registerCategory($request){
		$brand = Category::create($request);
		return $brand;
	}

	static function viewDetCategory($id){
		$brand = Category::where('id',$id)->first();
		return $brand;
	}

	static function editCategory($request,Category $id){
		$id->update($request);
	}

	static function deleteCategory(Category $id){
		$id->delete();
	}
}