<?php
namespace App\Http\Entities\Admin;
use App\Models\Role;

class RolesEntity{
	static function registerRole($request){
		$roles = Role::create($request);
		return $roles;
	}

	static function viewDetRole($id){
		$roles = Role::where('id',$id)->first();
		return $roles;
	}

	static function editRoles($request,Role $id){
		$id->update($request);
	}

	static function deleteRoles(Role $id){
		$id->delete();
	}
}