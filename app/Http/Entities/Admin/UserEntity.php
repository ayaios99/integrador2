<?php
namespace App\Http\Entities\Admin;
use App\User;
use App\Models\Favorite;
use App\Models\Reserve;

class UserEntity{
	static function registerUser($request){
		$user = User::create($request);
		return $user;
	}

	static function viewDetUser($id){
		$user = User::where('id',$id)->first();
		return $user;
	}

	static function editUser($request,User $id){
		$id->update($request);
	}

	static function deleteUser(User $id){
		$id->delete();
	}


	/* PROVIDER */

	static function viewDetProvider($id){
		$user = User::where('id',$id)->first();
		return $user;
	}


	/* API */

	static function createClassicUser($request){
		$user = User::create($request);
		return $user;
	}

	static function createAFavorite($request){
		$favorite = Favorite::create($request);
		return $favorite;
	}

	static function createAReserve($request){
		$reserve = Reserve::create($request);
		return $reserve;
	}

}