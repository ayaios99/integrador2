<?php
namespace App\Http\Entities\Admin;
use App\Models\Brand;

class BrandEntity{
	static function registerBrand($request){
		$brand = Brand::create($request);
		return $brand;
	}

	static function viewDetBrand($id){
		$brand = Brand::where('id',$id)->first();
		return $brand;
	}

	static function editBrand($request,Brand $id){
		$id->update($request);
	}

	static function deleteBrand(Brand $id){
		$id->delete();
	}
}