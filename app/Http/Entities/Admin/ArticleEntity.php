<?php
namespace App\Http\Entities\Admin;
use App\Models\Article;

class ArticleEntity{
	static function registerArticle($request){
		$article = Article::create($request);
		return $article;
	}

	static function viewDetArticle($id){
		$article = Article::where('id',$id)->first();
		return $article;
	}


	static function uniqueArticle($id){
		$last_url = Article::where('id',$id)->first();
		return $last_url;
	}

	static function editArticle($input,Article $id){
		$id->update($input);
	}

	static function deleteArticle(Article $id){
		$id->delete();
	}
}