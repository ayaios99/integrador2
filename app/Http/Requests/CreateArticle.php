<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateArticle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'description' =>'required|string',
            'price' => 'required|numeric',
            'state' => 'required|boolean',
            'stock' => 'required|numeric',
            'img_url' => 'required|file',
            'id_user' => 'required|numeric',
            'id_brand' => 'required|numeric',
            'id_category' => 'required|numeric',
            'id_catalogue' => 'required|numeric',
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Debe escrbir correctamente el campo del nombre',
            'description.required' => 'Debe escrbir correctamente el campo del descripción',
            'price' => 'Debe escrbir correctamente (numérico) el campo del Precio',
            'stock' => 'Debe escribir correctamente (numérico) el campo de stock',
            'img_url' => 'Inserte conrrectamente el archivo',
            'id_user' => 'Elegir correctamente al Proveedor',
            'id_brand' => 'Elegir correctamente al Marca',
            'id_category' => 'Elegir correctamente al Categoría',
            'id_catalogue' => 'Elegir correctamente al Catálogo',
        ];
    }
}
