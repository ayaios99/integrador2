<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|string',
            'email'=>'required|email',
            'telefono' =>'required|numeric',
            'document' => 'required|string|max:15',
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Debe escrbir correctamente el campo del nombre',
            'email.required' => 'Debe escrbir correctamente el campo del Email',
            'telefono.required' => 'Debe escrbir correctamente el campo del Teléfono',
            'document.required' => 'Debe escrbir correctamente el campo del Documento',
        ];
    }

}
