<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRole extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'=>'required|string',
            'description'=>'required|string',
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Debe escrbir correctamente el campo del nombre',
            'description.required' => 'Debe escrbir correctamente el campo del descripción',
        ];
    }
}
